package khyri.textgame.core.util;

import com.google.gson.*;
import khyri.textgame.core.triggers.Trigger;

import java.lang.reflect.Type;

public class TriggerDeserializer implements JsonDeserializer<Trigger> {

    @Override
    public Trigger deserialize(JsonElement jsonElement,
                               Type type,
                               JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        JsonPrimitive typeNameJson = (JsonPrimitive)jsonObject.get("type");
        String typeName = typeNameJson.getAsString();

        Class<?> deserializeTo;
        try {
            deserializeTo = Class.forName(typeName);
        } catch (ClassNotFoundException ex) {
            throw new JsonParseException(ex);
        }

        return jsonDeserializationContext.deserialize(jsonObject.get("instance"), deserializeTo);
    }
}
