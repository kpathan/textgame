package khyri.textgame.core.util;

import java.util.ArrayList;
import java.util.List;

public class GraphDescription<T> {
    private final List<T> vertices;
    private final List<Edge> edges;
    private final String start;

    GraphDescription(List<T> vertices, List<Edge> edges, String start) {
        this.vertices = vertices;
        this.edges = edges;
        this.start = start;
    }

    public List<T> getVertices() {
        return new ArrayList<>(vertices);
    }

    public List<Edge> getEdges() {
        return new ArrayList<>(edges);
    }

    public String getStart() {
        return start;
    }
}
