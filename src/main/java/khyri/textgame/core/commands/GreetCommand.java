package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

public class GreetCommand implements Command {
    private final String target;

    private GreetCommand(String target) {
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public static GreetCommand create(String paramString) throws InvalidCommandException {
        if(paramString.trim().isEmpty()) {
            throw new InvalidCommandException(GameUtility.usageMessage("greet npc"));
        }

        return new GreetCommand(paramString);
    }
}
