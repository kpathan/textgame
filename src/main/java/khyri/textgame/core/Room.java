package khyri.textgame.core;

import khyri.textgame.core.triggers.Trigger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Room extends GameValue {
    private final List<Item> items;
    private final List<Trigger> triggers;
    private final List<NonPlayerCharacter> npcs;

    public Room(String identifier,
                String description,
                List<Item> items,
                List<Trigger> triggers,
                List<NonPlayerCharacter> npcs) {
        super(identifier, description);
        this.items = new ArrayList<>(items);
        this.triggers = new ArrayList<>(triggers);
        this.npcs = new ArrayList<>(npcs);
    }

    public Room() {
        this("", "",
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList());
    }

    public static Room withId(String identifier) {
        return new Room(identifier, "",
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList());
    }

    public List<Item> getItems() {
        if (items == null) {
            return Collections.emptyList();
        }
        return new ArrayList<>(items);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public List<Trigger> getTriggers() {
        return new ArrayList<>(triggers);
    }

    public void removeTrigger(Trigger trigger) {
        triggers.remove(trigger);
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public List<NonPlayerCharacter> getNpcs() {
        return new ArrayList<>(npcs);
    }
}
