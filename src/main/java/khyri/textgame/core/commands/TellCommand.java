package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

public class TellCommand implements Command {
    private final String target;
    private final String message;

    private TellCommand(String target, String message) {
        this.target = target;
        this.message = message;
    }

    public String getTarget() {
        return target;
    }

    public String getMessage() {
        return message;
    }

    public static TellCommand create(String paramString) throws InvalidCommandException {
        final String USAGE = "tell target: message";
        if (paramString.trim().isEmpty()) {
            throw new InvalidCommandException(GameUtility.usageMessage(USAGE));
        }

        String[] params = paramString.split("\\s+", 2);

        if(params.length != 2) {
            throw new InvalidCommandException(GameUtility.usageMessage(USAGE));
        }

        final int TARGET = 0;
        final int MESSAGE = 1;

        if(!params[TARGET].endsWith(":")) {
            throw new InvalidCommandException(GameUtility.usageMessage(USAGE));
        }

        String target = params[TARGET].substring(0, params[TARGET].indexOf(":"));

        return new TellCommand(target, params[MESSAGE]);
    }
}
