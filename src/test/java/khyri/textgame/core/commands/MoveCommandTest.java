package khyri.textgame.core.commands;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoveCommandTest {
    @Test(expected = InvalidCommandException.class)
    public void create_emptyOnIncorrectPreposition() throws InvalidCommandException {
        MoveCommand command = MoveCommand.create("up yours");
    }

    @Test(expected = InvalidCommandException.class)
    public void create_emptyOnTooFewWords() throws InvalidCommandException {
        MoveCommand command = MoveCommand.create("test");
    }

    @Test
    public void create_moveCommandOnValidParams() throws InvalidCommandException {
        MoveCommand command = MoveCommand.create("to room");

        assertEquals("create did not set the \"to\" field.", "room", command.getTarget());
    }

    @Test
    public void create_multipleWordsAfterTo_createsMoveCommand() throws InvalidCommandException {
        MoveCommand command = MoveCommand.create("to the cave");

        assertEquals("create did not set to field properly.", "the cave",
                command.getTarget());
    }
}
