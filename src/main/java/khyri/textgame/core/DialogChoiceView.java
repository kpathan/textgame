package khyri.textgame.core;

import khyri.textgame.core.util.Node;

public class DialogChoiceView {
    String render(Node<Dialog.Choice> startingChoice) {
        StringBuilder view = new StringBuilder(startingChoice.getData().getResponse());

        for (Node<Dialog.Choice> choice :
                startingChoice.getConnectedNodes()) {
            view.append("\n> ");
            view.append(choice.getData().getSelection());
        }

        return view.toString();
    }
}
