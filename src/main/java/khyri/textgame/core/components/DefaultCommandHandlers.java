package khyri.textgame.core.components;

import com.google.common.eventbus.Subscribe;
import khyri.textgame.core.*;
import khyri.textgame.core.commands.*;
import khyri.textgame.core.events.MessageEvent;
import khyri.textgame.core.events.PlayerEnteredRoomEvent;
import khyri.textgame.core.triggers.Trigger;
import khyri.textgame.core.util.GameUtility;
import khyri.textgame.core.events.PlayerGotItemEvent;
import khyri.textgame.core.util.Node;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultCommandHandlers extends GameComponent {

    @Subscribe
    void onPlayerMove(MoveCommand moveCommand) {
        Optional<Node<Room>> roomToMoveTo =
                getWorld().getCurrentRoomNode().findConnectedNode(Room.withId(moveCommand.getTarget()));

        if (roomToMoveTo.isPresent()) {
            getWorld().setCurrentRoomNode(roomToMoveTo.get());
            GameUtility.displayRoom(getEventBus(), getWorld());
            List<Trigger> triggers = getWorld().getCurrentRoomNode().getData().getTriggers();
            getEventBus().post(new PlayerEnteredRoomEvent(triggers));
        } else {
            getEventBus().post(new MessageEvent(
                    String.format("There is no exit \"%s\"", moveCommand.getTarget()))
            );
        }
    }

    @Subscribe
    void onPlayerInventory(InventoryCommand inventoryCommand) {
        StringJoiner itemList = new StringJoiner(", ", "(", ")");
        getPlayer().getInventory().stream()
                .map(GameValue::getIdentifier).forEach(itemList::add);
        getEventBus().post(new MessageEvent(String.format("You are carrying: %s.", itemList)));
    }

    @Subscribe
    void onPlayerGetItemCommand(TryGetItemCommand tryGetItemCommand) {
        ifFoundInCollection(tryGetItemCommand.getItemIdentifier(),
                getWorld().getCurrentRoomNode().getData().getItems(),
                (matchingItem) -> {
                    getWorld().getCurrentRoomNode().getData()
                            .removeItem(matchingItem);
                    getPlayer().pickupItem(matchingItem);

                    List<Trigger> triggers = getWorld().getCurrentRoomNode().getData().getTriggers();
                    getEventBus().post(new PlayerGotItemEvent(matchingItem, triggers));
                    getEventBus().post(new MessageEvent(
                            String.format("Picked up %s.", tryGetItemCommand.getItemIdentifier())
                    ));
                });
    }

    @Subscribe
    void onPlayerDropItemCommand(TryDropItemCommand tryDropItemCommand) {
        ifFoundInCollection(tryDropItemCommand.getItemIdentifier(),
                getPlayer().getInventory(),
                (matchingItem) -> {
                    getPlayer().dropItem(matchingItem);
                    getWorld().getCurrentRoomNode().getData().addItem(matchingItem);
                    getEventBus().post(new MessageEvent(
                            String.format("Dropped %s.", tryDropItemCommand.getItemIdentifier())
                    ));
                });
    }

    @Subscribe
    void onPlayerLookCommand(LookCommand lookCommand) {
        if (!lookCommand.getTarget().isPresent()) {
            GameUtility.displayRoom(getEventBus(), getWorld());
        } else {
            String target = lookCommand.getTarget().get();
            List<Item> playerItemsAndRoomItems = Stream.concat(getWorld().getCurrentRoomNode().getData().getItems().stream(),
                    getPlayer().getInventory().stream()).collect(Collectors.toList());
            ifFoundInCollection(target, playerItemsAndRoomItems,
                    (foundItem) -> getEventBus().post(new MessageEvent(foundItem.getDescription())));
        }
    }

    @Subscribe
    void onPlayerGreetCommand(GreetCommand greetCommand) {
        List<NonPlayerCharacter> npcs = getWorld().getCurrentRoomNode().getData().getNpcs();
        ifFoundInCollection(greetCommand.getTarget(), npcs,
                (foundNpc) -> getEventBus().post(new MessageEvent(foundNpc.getDialog().start())));
    }

    @Subscribe
    void onPlayerSayCommand(TellCommand tellCommand) {
        List<NonPlayerCharacter> npcs = getWorld().getCurrentRoomNode().getData().getNpcs();
        ifFoundInCollection(tellCommand.getTarget(), npcs,
                (foundNpc) ->
                        getEventBus().post(new MessageEvent(foundNpc.getDialog().choose(tellCommand.getMessage()))));
    }

    private <T extends GameValue> void ifFoundInCollection(String identifier,
                                                           List<T> items,
                                                           Consumer<T> foundElementAction) {
        List<T> matchElements = items.stream()
                .filter(e -> Objects.equals(e.getIdentifier(), identifier))
                .collect(Collectors.toList());

        if (matchElements.isEmpty()) {
            getEventBus().post(new MessageEvent(
                    GameUtility.notPresentMessage(identifier))
            );
        } else {
            foundElementAction.accept(matchElements.get(0));
        }
    }
}
