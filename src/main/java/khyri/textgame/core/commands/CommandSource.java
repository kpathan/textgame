package khyri.textgame.core.commands;

public interface CommandSource {
    Command next() throws InvalidCommandException;
}
