package khyri.textgame.core;

import com.google.inject.Inject;
import khyri.textgame.core.util.Node;

public class TextWorld {
    private Node<Room> currentRoomNode;
    private final RoomView view;

    @Inject
    public TextWorld(Node<Room> startingRoomNode,
                     RoomView view) {
        this.currentRoomNode = startingRoomNode;
        this.view = view;
    }

    public String renderCurrentRoom() {
        return this.view.render(this.currentRoomNode);
    }

    public Node<Room> getCurrentRoomNode() {
        return this.currentRoomNode;
    }

    public void setCurrentRoomNode(Node<Room> roomToMoveTo) {
        this.currentRoomNode = roomToMoveTo;
    }
}
