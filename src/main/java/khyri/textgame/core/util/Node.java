package khyri.textgame.core.util;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Node<T> {
    private final T data;
    private final Set<Node<T>> connectedNodes;

    public Node(T data) {
        this.data = data;
        this.connectedNodes = new HashSet<>();
    }

    public Optional<Node<T>> findConnectedNode(T t) {
        return connectedNodes.stream()
                .filter(node -> node.getData().equals(t))
                .findFirst();
    }

    public void connectBidirectionallyTo(Node<T> other) {
        other.connectedNodes.add(this);
        connectedNodes.add(other);
    }

    public void connectTo(Node<T> other) {
        connectedNodes.add(other);
    }

    public Set<Node<T>> getConnectedNodes() {
        return new HashSet<>(connectedNodes);
    }

    public T getData() {
        return data;
    }
}
