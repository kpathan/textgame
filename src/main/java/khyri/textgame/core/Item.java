package khyri.textgame.core;

public class Item extends GameValue {
    public Item(String identifier, String description) {
        super(identifier, description);
    }

    public static Item withName(String identifier) {
        return new Item(identifier, "");
    }
}
