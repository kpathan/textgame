package khyri.textgame.core.triggers;

import khyri.textgame.core.Player;
import khyri.textgame.core.Room;
import khyri.textgame.core.events.MessageEvent;
import khyri.textgame.core.util.Node;

public class PlayerHasItemTrigger extends Trigger {

    private final String item;
    private final String message;
    private boolean isExpired = false;

    public PlayerHasItemTrigger(String item, String message, String identifier) {
        super(identifier, "");
        this.item = item;
        this.message = message;
    }

    @Override
    public boolean condition(Node<Room> room, Player player) {
        return player.getInventory().stream()
                .anyMatch(i -> i.getIdentifier().equals(item));
    }

    @Override
    public boolean isExpired() {
        return isExpired;
    }

    @Override
    public void trigger(Node<Room> room, Player player) {
        this.isExpired = true;
    }

    @Override
    public MessageEvent getMessage() {
        return new MessageEvent(message);
    }
}
