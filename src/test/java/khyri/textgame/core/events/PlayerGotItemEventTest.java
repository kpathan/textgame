package khyri.textgame.core.events;

import khyri.textgame.core.TestUtility;
import khyri.textgame.core.triggers.Trigger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class PlayerGotItemEventTest {
    @Test
    public void getTriggers_cannotMutateInternalList() {
        PlayerGotItemEvent playerGotItemEvent =
                new PlayerGotItemEvent(TestUtility.testItem("test"),
                        new ArrayList<>(Collections.singletonList(TestUtility.testTrigger("test"))));

        TestUtility.assertGetterImmutability("getTriggers caller can mutate internal list.", playerGotItemEvent::getTriggers);
    }

    @Test
    public void constructor_cannotMutateInternalList() {
        ArrayList<Trigger> list = new ArrayList<>(Collections.singletonList(TestUtility.testTrigger("test")));
        PlayerGotItemEvent playerGotItemEvent =
                new PlayerGotItemEvent((TestUtility.testItem("test")),
                        list);

        list.clear();

        assertEquals("constructor caller can mutate internal list.", 1, playerGotItemEvent.getTriggers().size());
    }
}
