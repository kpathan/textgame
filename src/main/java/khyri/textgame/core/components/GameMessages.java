package khyri.textgame.core.components;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import khyri.textgame.core.GameComponent;
import khyri.textgame.core.events.MessageEvent;

import java.io.PrintStream;

public class GameMessages extends GameComponent {
    private final PrintStream outputStream;

    @Inject
    public GameMessages(PrintStream outputStream) {
        this.outputStream = outputStream;
    }

    @Subscribe
    public void OnMessageCommand(MessageEvent message) {
        outputStream.println(message.toString() + "\n");
    }
}
