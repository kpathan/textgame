package khyri.textgame.core;

import khyri.textgame.core.util.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DialogTest {
    @Test
    public void begin_showsStartingDialog() {
        Dialog dialog =
                new Dialog("Wha?",
                        new DialogChoiceView(),
                        new Node<>(new Dialog.Choice("Hello")));


        String result = dialog.start();

        assertEquals("Begin returns incorrect result.", "Hello", result);
    }

    @Test
    public void choose_movesToNextDialogChoice() {
        Node<Dialog.Choice> choices =
                new Node<>(new Dialog.Choice("Hello"));
        choices.connectTo(new Node<>(new Dialog.Choice("Not bad", "How are you?")));

        Dialog dialog = new Dialog("", new DialogChoiceView(), choices);

        String result = dialog.choose("How are you?");

        assertEquals("choose returns incorrect result.", "Not bad", result);
    }
}
