package khyri.textgame.core.util;

import java.io.InputStream;
import java.util.Scanner;

public final class FileUtility {
    private FileUtility() {
    }

    public static String readAllContent(InputStream stream) {
        try(Scanner scanner = new Scanner(stream)) {
            scanner.useDelimiter("\\Z");
            return scanner.next();
        }
    }
}
