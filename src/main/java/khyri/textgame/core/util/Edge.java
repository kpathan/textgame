package khyri.textgame.core.util;

public class Edge {
    private final String from;
    private final String to;

    public Edge(String from, String to) {
        this.from = from;
        this.to = to;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }
}
