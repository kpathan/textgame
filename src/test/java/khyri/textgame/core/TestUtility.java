package khyri.textgame.core;

import khyri.textgame.core.triggers.Trigger;
import khyri.textgame.core.triggers.PlayerHasItemTrigger;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertTrue;

public class TestUtility {
    public static Room testRoom() {
        return TestUtility.testRoom("test", "", Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList());
    }

    public static Room testRoom(String identifier) {
        return TestUtility.testRoom(identifier, "", Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList());
    }

    public static Room testRoom(String identifier, String description) {
        return TestUtility.testRoom("test", description, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList());
    }

    public static Room testRoom(String identifier, String description, List<Item> items) {
        return TestUtility.testRoom("test", description, items, Collections.emptyList(), Collections.emptyList());
    }

    public static Room testRoom(String identifier, String description, List<Item> items, List<Trigger> triggers) {
        return TestUtility.testRoom("test", description, items, triggers, Collections.emptyList());
    }

    public static Room testRoom(String identifier, String description, List<Item> items, List<Trigger> triggers,
                                List<NonPlayerCharacter> npcs) {
        return new Room(identifier, description, items, triggers, npcs);
    }

    public static Trigger testTrigger(String name) {
        return new PlayerHasItemTrigger("test", "test", name);
    }

    public static Item testItem(String identifier) {
        return new Item(identifier, "");
    }

    public static <T> void assertGetterImmutability(String message, Supplier<List<T>> getter) {
        List<T> returned = getter.get();
        int originalSize = returned.size();

        returned.clear();

        assertTrue(message.trim().isEmpty() ?
                        "The supplied getter allows modification of internal list." :
                        message,
                getter.get().size() == originalSize);
    }

    public static NonPlayerCharacter testNpc(String identifer) {
        return new NonPlayerCharacter(identifer, "", null);
    }
}
