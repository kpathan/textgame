package khyri.textgame.core;

import khyri.textgame.core.util.Node;

import java.util.Objects;
import java.util.Optional;

public class Dialog {
    private String noSuchChoiceResponse;
    private DialogChoiceView view;
    private Node<Choice> choices;
    private Node<Choice> startingChoice;
    private boolean firstTimeStarting = true;

    public Dialog(String noSuchChoiceResponse,
                  DialogChoiceView view,
                  Node<Choice> choices) {
        this.noSuchChoiceResponse = noSuchChoiceResponse;
        this.view = view;
        this.choices = choices;
    }

    public Dialog() {
        this.noSuchChoiceResponse = "";
        this.view = new DialogChoiceView();
        this.choices = null;
    }

    public String start() {
        // needed to use a flag here instead of performing this in the constructor
        // because deserialization bypasses the constructor
        if(firstTimeStarting) {
            startingChoice = choices;
            firstTimeStarting = false;
        }

        choices = startingChoice;
        return show();
    }

    public String choose(String choice) {
        Optional<Node<Choice>> foundChoice =
                choices.findConnectedNode(Choice.withValue(choice));

        if(foundChoice.isPresent()) {
            this.choices = foundChoice.get();
            return show();
        }

        return noSuchChoiceResponse;
    }

    private String show() {
        return this.view.render(this.choices);
    }

    public static class Choice {
        private final String selection;
        private final String response;

        public Choice(String response, String selection) {
            this.response = response;
            this.selection = selection;
        }

        public Choice(String response) {
            this(response, "");
        }

        public String getSelection() {
            return selection;
        }

        public String getResponse() {
            return response;
        }

        public static Choice withValue(String value) {
            return new Choice("", value);
        }

        @Override
        public String toString() {
            return response;
        }

        @Override
        public int hashCode() {
            return response.hashCode();
        }

        @Override
        public boolean equals(Object other) {
            if(!(other instanceof Choice)) {
                return false;
            }

            if(other == this) {
                return true;
            }

            Choice otherChoice = (Choice)other;

            return Objects.equals(this.getSelection(), otherChoice.getSelection());
        }
    }
}
