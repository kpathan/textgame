package khyri.textgame.core.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonIncluderTest {
    @Test
    public void resolveFileReferences_includesJsonFromAnotherFile() {
        JsonIncluder jsonIncluder = new JsonIncluder(new Gson());

        String jsonWithFileReference = "{\"test\":{ \"$fileRef\":\"/test.json\"}}";
        String expectedJsonResult = "{\"test\":{\"id\":\"test\"}}";

        String result = jsonIncluder.resolveFileReferences(jsonWithFileReference);

        assertEquals("resolveFileReferences did not include json file content", expectedJsonResult, result);
    }

    @Test
    public void resolveFileReferences_handlesArrays() {
        JsonIncluder jsonIncluder = new JsonIncluder(new Gson());

        String jsonWithFileReference = "{\"test\":[{ \"$fileRef\":\"/test.json\"}]}";
        String expectedJsonResult = "{\"test\":[{\"id\":\"test\"}]}";

        String result = jsonIncluder.resolveFileReferences(jsonWithFileReference);

        assertEquals("resolveFileReferences did not include json file content", expectedJsonResult, result);
    }

    @Test
    public void resolveFileReferences_resolvesMultipleFileReferences() {
        JsonIncluder jsonIncluder = new JsonIncluder(new Gson());

        String jsonWithFileReference = "{\"test\":[{ \"$fileRef\":\"/test.json\"},{ \"$fileRef\":\"/test.json\"}]}";
        String expectedJsonResult = "{\"test\":[{\"id\":\"test\"},{\"id\":\"test\"}]}";

        String result = jsonIncluder.resolveFileReferences(jsonWithFileReference);

        assertEquals("resolveFileReferences did not include json file content", expectedJsonResult, result);
    }
}
