package khyri.textgame.core.commands;

public class TryDropItemCommand extends ManageItemCommand {
    private TryDropItemCommand(String itemIdentifier) {
        super(itemIdentifier, "drop item");
    }

    public static CommandFactory<String, TryDropItemCommand> factory() {
        return ManageItemCommand.factoryFor(TryDropItemCommand.class,
                TryDropItemCommand::new);
    }
}
