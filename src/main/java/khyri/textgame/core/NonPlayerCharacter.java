package khyri.textgame.core;

public class NonPlayerCharacter extends GameValue {
    private final Dialog dialog;

    public NonPlayerCharacter(String identifier,
                              String description,
                              Dialog dialog)
    {
        super(identifier, description);
        this.dialog = dialog;
    }

    public Dialog getDialog() {
        return dialog;
    }
}
