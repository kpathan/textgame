package khyri.textgame.core.commands;

public class TryGetItemCommand extends ManageItemCommand {
    private TryGetItemCommand(String itemIdentifier) {
        super(itemIdentifier, "(take | get) item");
    }

    public static CommandFactory<String, TryGetItemCommand> factory() {
        return ManageItemCommand.factoryFor(TryGetItemCommand.class,
                TryGetItemCommand::new);
    }
}
