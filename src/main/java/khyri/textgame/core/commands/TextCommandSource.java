package khyri.textgame.core.commands;

import java.util.Map;
import java.util.Scanner;

public class TextCommandSource implements CommandSource {
    private final Map<String, CommandFactory<String, ? extends Command>> commandRegistry;
    private final Scanner commandInput;

    public TextCommandSource(Map<String, CommandFactory<String, ? extends Command>> textCommandRegistry,
                             Scanner commandInput) {
        this.commandRegistry = textCommandRegistry;
        this.commandInput = commandInput;
    }

    @Override
    public Command next() throws InvalidCommandException {
        final int COMMAND = 0;
        final int PARAM_STRING = 1;

        String input = commandInput.nextLine();
        String[] inputParts = input.split("\\s+", 2);

        CommandFactory<String, ? extends Command> commandFactory =
                commandRegistry.get(inputParts[COMMAND]);

        if(commandFactory == null) {
            throw new InvalidCommandException(
                    String.format("Command \"%s\" does not exist.", inputParts[COMMAND]));
        }

        return commandFactory.create(inputParts.length == 2 ?  inputParts[PARAM_STRING] : "");
    }
}
