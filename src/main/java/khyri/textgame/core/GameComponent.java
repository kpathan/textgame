package khyri.textgame.core;

import com.google.common.eventbus.EventBus;

public abstract class GameComponent {
    private TextWorld world;
    private Player player;
    private EventBus eventBus;

    void setWorld(TextWorld world) {
        this.world = world;
    }

    void setPlayer(Player player) {
        this.player = player;
    }

    void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public final TextWorld getWorld() {
        return this.world;
    }

    public final Player getPlayer() {
        return this.player;
    }

    public EventBus getEventBus() {
        return eventBus;
    }
}
