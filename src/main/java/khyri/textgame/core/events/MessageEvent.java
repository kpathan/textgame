package khyri.textgame.core.events;

public class MessageEvent {
    private final String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("%s", this.message);
    }
}
