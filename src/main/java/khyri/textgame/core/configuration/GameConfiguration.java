package khyri.textgame.core.configuration;

import com.google.common.eventbus.EventBus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import khyri.textgame.core.Dialog;
import khyri.textgame.core.GameComponent;
import khyri.textgame.core.Room;
import khyri.textgame.core.commands.*;
import khyri.textgame.core.components.DefaultCommandHandlers;
import khyri.textgame.core.components.DefaultEventHandlers;
import khyri.textgame.core.components.GameMessages;
import khyri.textgame.core.components.TriggersComponent;
import khyri.textgame.core.triggers.Trigger;
import khyri.textgame.core.util.*;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

public class GameConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        bind(PrintStream.class).toInstance(System.out);
        bind(EventBus.class).toInstance(new EventBus());
        bind(Gson.class).toInstance(new GsonBuilder()
                .registerTypeAdapter(Trigger.class, new TriggerDeserializer())
                .registerTypeAdapter(new TypeToken<Node<Room>>() {
                        }.getType(),
                        new NodeDeserializer<>(Room.class, Room::getIdentifier, Node::connectBidirectionallyTo))
                .registerTypeAdapter(new TypeToken<Node<Dialog.Choice>>(){}.getType(),
                        new NodeDeserializer<>(Dialog.Choice.class, Dialog.Choice::getSelection, Node::connectTo))
                .create());
        bind(String.class).annotatedWith(Names.named("roomFile")).toInstance("/rooms.json");
    }

    @Provides
    Node<Room> getInitialRoom(@Named("roomFileStream") InputStream roomFileStream,
                              JsonIncluder jsonIncluder,
                              Gson gson) {
        String partialJson = FileUtility.readAllContent(roomFileStream);
        String fullJson = jsonIncluder.resolveFileReferences(partialJson);
        return gson.fromJson(fullJson,
                new TypeToken<Node<Room>>(){}.getType());
    }

    @Provides
    @Named("roomFileStream")
    InputStream getRoomFileStream() {
        return GameConfiguration.class.getResourceAsStream("/rooms.json");
    }

    @Provides
    CommandSource getCommandSource() {
        Map<String, CommandFactory<String, ? extends Command>> playerCommandRegistry =
                new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        playerCommandRegistry.put("move", MoveCommand::create);
        playerCommandRegistry.put("get", TryGetItemCommand.factory());
        playerCommandRegistry.put("take", TryGetItemCommand.factory());
        playerCommandRegistry.put("drop", TryDropItemCommand.factory());
        playerCommandRegistry.put("greet", GreetCommand::create);
        playerCommandRegistry.put("tell", TellCommand::create);
        playerCommandRegistry.put("quit", GameEndCommand::create);
        playerCommandRegistry.put("exit", GameEndCommand::create);
        playerCommandRegistry.put("inventory", InventoryCommand::create);
        playerCommandRegistry.put("look", LookCommand::create);
        return new TextCommandSource(playerCommandRegistry, new Scanner(System.in));
    }

    @Provides
    @Named("gameComponents")
    Set<GameComponent> getGameComponents(GameMessages gameMessages,
                                         DefaultEventHandlers defaultEventHandlers,
                                         DefaultCommandHandlers defaultCommandHandlers,
                                         TriggersComponent triggersComponent) {
        Set<GameComponent> gameComponents = new HashSet<>();
        gameComponents.add(gameMessages);
        gameComponents.add(defaultEventHandlers);
        gameComponents.add(defaultCommandHandlers);
        gameComponents.add(triggersComponent);

        return gameComponents;
    }
}
