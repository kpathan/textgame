package khyri.textgame.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import khyri.textgame.core.triggers.Trigger;
import khyri.textgame.core.events.MessageEvent;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TriggerDeserializerTest {
    @Test
    public void test() {
        String triggerJson = "{ type: \"khyri.textgame.core.util.TriggerDeserializerTest$TriggerImpl\", " +
                "instance: { \"field\": \"test\"} }";

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Trigger.class, new TriggerDeserializer());
        Gson gson = gsonBuilder.create();

        Trigger t = gson.fromJson(triggerJson, Trigger.class);
        assertEquals("Deserializer did not deserialize field", "test", ((TriggerImpl)t).field);
    }

    public static class TriggerImpl extends Trigger {
        private final String field;

        public TriggerImpl(String field) {
            super("", "");
            this.field = field;
        }

        @Override
        public MessageEvent getMessage() {
            return new MessageEvent("test");
        }

        public String getField() {
            return field;
        }
    }
}
