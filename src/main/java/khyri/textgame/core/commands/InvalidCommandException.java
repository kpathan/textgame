package khyri.textgame.core.commands;

public class InvalidCommandException extends Exception {
    InvalidCommandException(String message) {
        super(message);
    }
}
