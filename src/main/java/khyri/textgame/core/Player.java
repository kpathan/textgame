package khyri.textgame.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Player {
    private final List<Item> inventory;

    Player(List<Item> startingInventory) {
        this.inventory = new ArrayList<>(startingInventory);
    }

    public Player() {
        this(new ArrayList<>());
    }

    public final List<Item> getInventory() {
        return new ArrayList<>(inventory);
    }

    public void dropItem(Item item) {
        inventory.remove(item);
    }

    public void pickupItem(Item item) {
        inventory.add(item);
    }
}
