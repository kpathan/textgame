package khyri.textgame.core;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import khyri.textgame.core.commands.Command;
import khyri.textgame.core.commands.CommandSource;
import khyri.textgame.core.commands.GameEndCommand;
import khyri.textgame.core.commands.InvalidCommandException;
import khyri.textgame.core.configuration.GameConfiguration;
import khyri.textgame.core.events.GameStartEvent;
import khyri.textgame.core.events.MessageEvent;

import java.util.Set;

public class Game {
    private final CommandSource commandSource;
    private final EventBus eventBus;
    private final TextWorld world;
    private final Player player;
    private final Set<GameComponent> gameComponents;
    private boolean shouldEnd = false;

    @Inject
    Game(CommandSource commandSource,
         EventBus eventBus,
         TextWorld world,
         Player player,
         @Named("gameComponents") Set<GameComponent> gameComponents) {

        this.commandSource = commandSource;
        this.eventBus = eventBus;
        this.world = world;
        this.player = player;
        this.gameComponents = gameComponents;
    }

    public static Game withDefaultConfiguration() {
        Injector injector = Guice.createInjector(new GameConfiguration());

        return injector.getInstance(Game.class);
    }

    public void start() {
        eventBus.register(this);
        initializeComponents(gameComponents);
        registerComponents(gameComponents);

        eventBus.post(new GameStartEvent());
        while (!shouldEnd) {
            try {
                Command nextCommand = commandSource.next();
                eventBus.post(nextCommand);
            } catch (InvalidCommandException invalidCommandException) {
                eventBus.post(new MessageEvent(invalidCommandException.getMessage()));
            }
        }
    }

    @Subscribe
    void end(GameEndCommand gameEndCommand) {
        this.shouldEnd = true;
    }

    private void initializeComponents(Set<GameComponent> gameComponents) {
        gameComponents.forEach(c -> c.setPlayer(this.player));
        gameComponents.forEach(c -> c.setWorld(this.world));
        gameComponents.forEach(c -> c.setEventBus(this.eventBus));
    }

    private void registerComponents(Set<GameComponent> gameComponents) {
        gameComponents.forEach(eventBus::register);
    }
}
