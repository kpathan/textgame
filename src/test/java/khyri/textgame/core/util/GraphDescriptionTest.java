package khyri.textgame.core.util;

import khyri.textgame.core.Room;
import khyri.textgame.core.TestUtility;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class GraphDescriptionTest {
    @Test
    public void getVertices_cannotMutateInternalList() {
        GraphDescription<Room> graphDescription = new GraphDescription<>(
                new ArrayList<>(
                        Collections.singletonList(TestUtility.testRoom())),
                new ArrayList<>(
                        Collections.singletonList(new Edge("test", "test2"))),
                "test");

        TestUtility.assertGetterImmutability("getVertices caller can mutate internal list.",
                graphDescription::getVertices);
    }

    @Test
    public void getEdges_cannotMutateInternalList() {
        GraphDescription<Room> graphDescription = new GraphDescription<>(
                new ArrayList<>(
                        Collections.singletonList(TestUtility.testRoom())),
                new ArrayList<>(
                        Collections.singletonList(new Edge("test", "test2"))),
                "test");

        TestUtility.assertGetterImmutability("getEdges caller can mutate internal list.",
                graphDescription::getEdges);
    }
}
