package khyri.textgame.core.util;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GraphBuilder<T> {
    private final GraphDescription<T> graphDescription;
    private final Function<T, String> getIdentifier;
    private final BiConsumer<Node<T>, Node<T>> connectNodes;

    public GraphBuilder(GraphDescription<T> graphDescription, Function<T, String> getIdentifier,
                        BiConsumer<Node<T>, Node<T>> connectNodes) {
        this.graphDescription = graphDescription;
        this.getIdentifier = getIdentifier;
        this.connectNodes = connectNodes;
    }

    public Node<T> buildNodes() {
        Map<String, Node<T>> allRoomNodes = getNodeMap(graphDescription);

        Map<String, List<Edge>> edgesGroupedByFromValue =
                getEdgesGroupedByFrom(graphDescription);

        for (String key :
                edgesGroupedByFromValue.keySet()) {
            for (Edge c :
                    edgesGroupedByFromValue.get(key)) {

                Node<T> fromNode = allRoomNodes.get(key);
                Node<T> toNode = allRoomNodes.get(c.getTo());

                if (fromNode == null) {
                    throw CreateRoomConnectionException(key);
                }
                if (toNode == null) {
                    throw CreateRoomConnectionException(c.getTo());
                }

                connectNodes.accept(fromNode, toNode);
            }
        }

        String start = graphDescription.getStart();
        Node<T> nodeToReturn = allRoomNodes.get(start);

        if (nodeToReturn == null) {
            throw CreateStartUndefinedException(start);
        }

        return nodeToReturn;
    }

    private Map<String, Node<T>> getNodeMap(GraphDescription<T> graphDescription) {
        return graphDescription.getVertices()
                .stream()
                .collect(Collectors.toMap(getIdentifier,
                        Node::new));
    }

    private Map<String, List<Edge>> getEdgesGroupedByFrom(GraphDescription<T> graphDescription) {
        return graphDescription.getEdges()
                .stream()
                .collect(Collectors.groupingBy(Edge::getFrom));
    }

    private static IllegalStateException CreateRoomConnectionException(String edgeIdentifier) {
        return new IllegalStateException(
                String.format("Edge references vertex with identifier \"%s\" which is not defined.",
                        edgeIdentifier));
    }

    private static IllegalStateException CreateStartUndefinedException(String start) {
        return new IllegalStateException(
                String.format("Start references vertex with identifier \"%s\" which is not defined.",
                        start));
    }
}
