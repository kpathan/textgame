package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

public class InventoryCommand implements Command {
    private InventoryCommand() {
    }

    public static InventoryCommand create(String paramString)
            throws InvalidCommandException {
        if (!paramString.trim().isEmpty()) {
            throw new InvalidCommandException(GameUtility.usageMessage("inventory"));
        }

        return new InventoryCommand();
    }
}
