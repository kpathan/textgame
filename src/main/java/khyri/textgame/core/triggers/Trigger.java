package khyri.textgame.core.triggers;

import khyri.textgame.core.GameValue;
import khyri.textgame.core.Player;
import khyri.textgame.core.Room;
import khyri.textgame.core.events.MessageEvent;
import khyri.textgame.core.util.Node;

public abstract class Trigger extends GameValue {
    public Trigger(String identifier, String description) {
        super(identifier, description);
    }

    public boolean condition(Node<Room> room, Player player) {
        return true;
    }

    public boolean isExpired() {
        return false;
    }

    public void trigger(Node<Room> room, Player player) {
    }

    public abstract MessageEvent getMessage();

    public static Trigger withName(String name) {
        return new Trigger(name, "") {
            @Override
            public MessageEvent getMessage() {
                return null;
            }
        };
    }
}
