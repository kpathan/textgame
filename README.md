# Text Game Engine #

This project was motivated by my desire to gain familiarity with the Java ecosystem.

### What does it do? ###

The purpose of this project is to provide a framework for quickly creating a text-based adventure game in a declarative manner using human-readable files (JSON). In addition, the framework strives to be easily extensible.

### How do I get set up? ###

The project uses Maven as a build / dependency management system. It conforms to standard Maven build conventions. 

After installing Maven, simply clone the repo and run `mvn install` to build, test, and package the system.

### Examples ###

In a text game, the world is made up of entities such as rooms and items. These entities are defined in a JSON file that represents a graph (nodes, vertices, etc.) The JSON graph syntax is as follows:
```
{
  "vertices": [
    {
      "identifier": "your bedroom",
      "description": "It's a disheveled mess.",
      "items": [
        {
          "identifier": "pencil",
          "description": "A normal pencil."
        }
      ]
    },
    {
      "identifier": "the living room",
      "description": "Food and trash are strewn about."
    }
  ],
  "edges": [
    {
      "from": "your bedroom",
      "to": "the living room"
    }
  ],
  "start": "your bedroom"
}
```

By default, if a file in the above format is named rooms.json and included on the classpath, the JSON object will be deserialized to generate a graph of rooms in-game.

To start the game include the following code in the main method:

```
import khyri.textgame.core.Game;

public class App {
    public static void main(String[] args) {
        Game.withDefaultConfiguration().start();
    }
}
```

Given the above defined rooms.json, the following output will be produced:

```
You are in your bedroom.
It's a disheveled mess.
Items: (pencil)
There is 1 exit:
> the living room
```

The player interacts with the world using a series of commands that are typed into the console. There are a number of built-in commands. For instance:

+ **inventory** - lists the currently held items.
+ **look [at]** - prints the room description again, or, given a target such as an item, prints a description of the item (i.e. look at pencil).
+ **move to** - given a target room, if the room exists, moves the player to that room and prints the room info.
+ **take | get** - moves the target item to the player's inventory.
+ **drop** - moves the target item from the player's inventory to the room.