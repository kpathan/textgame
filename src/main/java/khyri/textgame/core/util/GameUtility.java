package khyri.textgame.core.util;

import com.google.common.eventbus.EventBus;
import khyri.textgame.core.TextWorld;
import khyri.textgame.core.events.MessageEvent;

public final class GameUtility {
    private GameUtility() {
    }

    public static String usageMessage(String usage) {
        return String.format("Invalid command. Correct usage: %s.", usage);
    }

    public static String notPresentMessage(String thing) {
        return String.format("There is no \"%s\" here.", thing);
    }

    public static void displayRoom(EventBus eventBus, TextWorld world) {
        eventBus.post(new MessageEvent(world.renderCurrentRoom()));
    }
}
