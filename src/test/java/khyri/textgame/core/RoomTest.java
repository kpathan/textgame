package khyri.textgame.core;

import static org.junit.Assert.*;

import khyri.textgame.core.triggers.Trigger;
import org.junit.Test;

import java.util.*;


public class RoomTest {
    @Test
    public void equals_equalityByIdentifier() {
        Room room1 = TestUtility.testRoom("Cave");
        Room room2 = TestUtility.testRoom("Cave");

        assertTrue("Rooms should be equal based on identifier.", Objects.equals(room1, room2));
    }

    @Test
    public void equals_equalityByIdentifierInverse(){
        Room room1 = TestUtility.testRoom("Cave");
        Room room2 = TestUtility.testRoom("Field");

        assertFalse("Rooms with different identifiers are not equal.", Objects.equals(room1, room2));
    }

    @Test
    public void hashcode_hashCodeByIdentifier() {
        Room room1 = TestUtility.testRoom("Cave");
        Room room2 = TestUtility.testRoom("Cave");

        assertTrue("Rooms with same identifier do not hash the same.", room1.hashCode() == room2.hashCode());
    }

    @Test
    public void getItems_cannotMutateInternalList() {
        Room room1 =
                TestUtility.testRoom("test", "description",
                        new ArrayList<>(Collections.singletonList(TestUtility.testItem("item"))));

        TestUtility.assertGetterImmutability("getItems caller can mutate internal list.", room1::getItems);
    }

    @Test
    public void getTriggers_cannotMutateInternalList() {
        Room room1 =
                TestUtility.testRoom("test", "description",
                        Collections.emptyList(),
                        new ArrayList<>(Collections.singletonList(TestUtility.testTrigger("test"))));

        TestUtility.assertGetterImmutability("getTriggers caller can mutate internal list.", room1::getTriggers);
    }

    @Test
    public void getNpcs_cannotMutateInternalList() {
        Room room1 =
                TestUtility.testRoom("test", "description",
                        Collections.emptyList(),
                        Collections.emptyList(),
                        new ArrayList<>(Collections.singletonList(TestUtility.testNpc("test"))));

        TestUtility.assertGetterImmutability("getNpcs caller can mutate internal list.", room1::getNpcs);
    }

    @Test
    public void constructor_cannotMutateInternalItemList() {
        ArrayList<Item> items = new ArrayList<>(Collections.singletonList(TestUtility.testItem("test")));
        Room room1 =
                TestUtility.testRoom("test", "description", items);

        items.clear();

        assertEquals("constructor caller can mutate internal list.", 1, room1.getItems().size());
    }

    @Test
    public void constructor_cannotMutateInternalTriggerList() {
        ArrayList<Trigger> triggers = new ArrayList<>(Collections.singletonList(TestUtility.testTrigger("test")));
        Room room1 =
                TestUtility.testRoom("test", "description", Collections.emptyList(), triggers);

        triggers.clear();

        assertEquals("constructor caller can mutate internal list.", 1, room1.getTriggers().size());
    }

    @Test
    public void constructor_cannotMutateInternalNpcList() {
        ArrayList<NonPlayerCharacter> npcs = new ArrayList<>(
                Collections.singletonList(TestUtility.testNpc("test")));
        Room room1 =
                TestUtility.testRoom("test", "description", Collections.emptyList(), Collections.emptyList(),
                        npcs);

        npcs.clear();

        assertEquals("constructor caller can mutate internal list.", 1, room1.getNpcs().size());
    }

    @Test
    public void removeItem_removesItem() {
        Room room1 =
                TestUtility.testRoom("test", "description",
                        new ArrayList<>(Collections.singletonList(TestUtility.testItem("item"))));

        room1.removeItem(Item.withName("item"));

        assertEquals("removeItem did not remove item.", 0, room1.getItems().size());
    }

    @Test
    public void removeTrigger_removesTrigger() {
        Room room1 =
                TestUtility.testRoom("test", "description",
                        Collections.emptyList(),
                        new ArrayList<>(Collections.singletonList(TestUtility.testTrigger("test"))));

        room1.removeTrigger(Trigger.withName("test"));

        assertEquals("removeTrigger did not remove trigger.", 0, room1.getTriggers().size());
    }
}
