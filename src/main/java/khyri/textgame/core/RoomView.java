package khyri.textgame.core;

import khyri.textgame.core.util.Node;

import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

public class RoomView {
    public String render(Node<Room> roomNode) {
        StringBuilder stringBuilder = renderLocation(roomNode);

        appendDescription(roomNode, stringBuilder);
        appendItems(roomNode, stringBuilder);
        appendNpcs(roomNode, stringBuilder);
        appendExits(roomNode, stringBuilder);

        return stringBuilder.toString();
    }

    private StringBuilder renderLocation(Node<Room> roomNode) {
        StringBuilder stringBuilder = new StringBuilder("\nYou are in ");
        stringBuilder.append(roomNode.getData().getIdentifier());
        stringBuilder.append(".\n");
        return stringBuilder;
    }

    private void appendDescription(Node<Room> roomNode, StringBuilder appendTo) {
        appendTo.append(roomNode.getData().getDescription());
        appendTo.append("\n");
    }

    private void appendItems(Node<Room> roomNode, StringBuilder appendTo) {
        appendTo.append("Items: ");
        StringJoiner joiner = new StringJoiner(", ", "(", ")");
        roomNode.getData().getItems()
                .stream()
                .map(Item::getIdentifier)
                .forEach(joiner::add);
        appendTo.append(joiner.toString());
        appendTo.append("\n");
    }

    private void appendNpcs(Node<Room> roomNode, StringBuilder appendTo) {
        List<NonPlayerCharacter> npcs = roomNode.getData().getNpcs();
        if(npcs.size() == 0) {
            return;
        }
        appendTo.append("There are people here: ");
        StringJoiner joiner = new StringJoiner(", ", "(", ")");
        npcs.stream()
                .map(NonPlayerCharacter::getIdentifier)
                .forEach(joiner::add);
        appendTo.append(joiner.toString());
        appendTo.append("\n");
    }

    private void appendExits(Node<Room> roomNode, StringBuilder appendTo) {
        appendTo.append("There ");
        Set<Node<Room>> connectedNodes = roomNode.getConnectedNodes();
        appendTo.append(connectedNodes.size() == 1 ? "is " : "are ");
        appendTo.append(connectedNodes.size());
        appendTo.append(connectedNodes.size() == 1 ? " exit:" : " exits:");

        connectedNodes.stream()
                .map(Node::getData)
                .forEach((data) -> {
                    appendTo.append("\n> ");
                    appendTo.append(data.getIdentifier());
                });
    }
}
