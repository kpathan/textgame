package khyri.textgame.core.util;

public interface View<T> {
    void set(T node);
    String render();
}


