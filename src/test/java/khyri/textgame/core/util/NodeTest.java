package khyri.textgame.core.util;

import khyri.textgame.core.Room;
import khyri.textgame.core.TestUtility;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class NodeTest {
    @Test
    public void connectBidirectionallyTo_connectsNodesToEachOther() {
        Node<Room> node1 = new Node<>(TestUtility.testRoom("Room1"));
        Node<Room> node2 = new Node<>(TestUtility.testRoom("Room2"));
        node1.connectBidirectionallyTo(node2);

        assertTrue("connectBidirectionallyTo: node1 was not connected to node2.",
                node1.findConnectedNode(TestUtility.testRoom("Room2")).isPresent());
        assertTrue("connectBidirectionallyTo: node2 was not connected to node1.",
                node2.findConnectedNode(TestUtility.testRoom("Room1")).isPresent());
    }

    @Test
    public void connectTo_connectsNodesToEachOther() {
        Node<Room> node1 = new Node<>(TestUtility.testRoom("Room1"));
        Node<Room> node2 = new Node<>(TestUtility.testRoom("Room2"));
        node1.connectTo(node2);

        assertTrue("connectTo: node1 was not connected to node2.",
                node1.findConnectedNode(TestUtility.testRoom("Room2")).isPresent());
    }

    @Test
    public void connectBidirectionallyTo_doesNotConnectNodeTwice() {
        Node<Room> node1 = new Node<>(TestUtility.testRoom("Room1"));
        Node<Room> node2 = new Node<>(TestUtility.testRoom("Room2"));

        node1.connectBidirectionallyTo(node2);
        node2.connectBidirectionallyTo(node1);

        assertTrue("Same node connected twice to one node.", node2.getConnectedNodes().size() == 1);
    }

    @Test
    public void findConnectedNode_returnsNodeIfFound() {
        Node<Room> otherNode = new Node<>(TestUtility.testRoom("toFind"));
        Node<Room> parentNode = new Node<>(TestUtility.testRoom());
        parentNode.connectBidirectionallyTo(otherNode);

        Optional<Node<Room>> foundNode = parentNode.findConnectedNode(TestUtility.testRoom("toFind"));

        assertTrue("findConnectedNode did not find a node.", foundNode.isPresent());
        assertTrue("findConnectedNode did not find the correct node.",
                foundNode.get().getData().equals(TestUtility.testRoom("toFind")));
    }
}
