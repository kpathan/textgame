package khyri.textgame.core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class PlayerTest {
    @Test
    public void getInventory_cannotMutateInternalList() {
        ArrayList<Item> inventory = new ArrayList<>();
        inventory.add(TestUtility.testItem("test"));
        Player p = new Player(inventory);

        TestUtility.assertGetterImmutability("getInventory caller can mutate internal list", p::getInventory);
    }

    @Test
    public void constructor_cannotMutateInternalList() {
        ArrayList<Item> inventory = new ArrayList<>(Collections.singletonList(TestUtility.testItem("test")));
        Player p = new Player(inventory);

        inventory.clear();

        assertEquals("constructor caller can mutate inventory.", 1, p.getInventory().size());
    }
}
