package khyri.textgame.core;

public class App {
    public static void main(String[] args) {
        Game.withDefaultConfiguration().start();
    }
}
