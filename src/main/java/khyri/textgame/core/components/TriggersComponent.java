package khyri.textgame.core.components;

import com.google.common.eventbus.Subscribe;
import khyri.textgame.core.GameComponent;
import khyri.textgame.core.events.PlayerEnteredRoomEvent;
import khyri.textgame.core.triggers.Trigger;
import khyri.textgame.core.events.PlayerGotItemEvent;

import java.util.List;

public class TriggersComponent extends GameComponent {
    @Subscribe
    void onPlayerEntersRoom(PlayerEnteredRoomEvent playerEnteredRoomEvent) {
        pullTriggers(playerEnteredRoomEvent.getTriggers());
    }

    @Subscribe
    void onPlayerGetsItem(PlayerGotItemEvent playerGotItemEvent) {
        pullTriggers(playerGotItemEvent.getTriggers());
    }

    private void pullTriggers(List<Trigger> triggers) {
        for (Trigger trigger :
                triggers) {
            if (trigger.condition(getWorld().getCurrentRoomNode(), getPlayer())) {
                trigger.trigger(getWorld().getCurrentRoomNode(), getPlayer());
                getEventBus().post(trigger.getMessage());
                if(trigger.isExpired()) {
                    getWorld().getCurrentRoomNode().getData().removeTrigger(trigger);
                }
            }
        }
    }
}
