package khyri.textgame.core.commands;

import java.util.Optional;

public class LookCommand implements Command {
    private final Optional<String> target;

    private LookCommand(Optional<String> target) {
        this.target = target;
    }

    private LookCommand() {
        this(Optional.empty());
    }

    public Optional<String> getTarget() {
        return target;
    }

    public static LookCommand create(String paramString) {
        if (paramString.trim().isEmpty()) {
            return new LookCommand();
        }

        String[] params = paramString.split("\\s+", 2);

        if(params.length == 2) {
            if(params[0].equals("at")) {
                return new LookCommand(Optional.of(params[1]));
            }
            return new LookCommand(Optional.of(paramString));
        }

        return new LookCommand(Optional.of(paramString));
    }
}
