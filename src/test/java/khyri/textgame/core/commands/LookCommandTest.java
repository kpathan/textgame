package khyri.textgame.core.commands;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class LookCommandTest {
    @Test
    public void create_preposition_createsCorrectCommand() {
        LookCommand lookCommand = LookCommand.create("at wall");

        assertEquals("create did not create correct LookCommand when given target with preposition.",
                "wall", lookCommand.getTarget().get());
    }

    @Test
    public void create_prepositionAndMultipleWordTarget_createsCorrectCommand() {
        LookCommand lookCommand = LookCommand.create("at large wall");

        assertEquals("create did not create correct LookCommand when given multiple word target and preposition.",
                "large wall", lookCommand.getTarget().get());
    }

    @Test
    public void create_noPreposition_createsCorrectCommand() {
        LookCommand lookCommand = LookCommand.create("wall");

        assertEquals("create did not create correct LookCommand.",
                "wall", lookCommand.getTarget().get());
    }

    @Test
    public void create_multipleWordTarget_createsCorrectCommand() {
        LookCommand lookCommand = LookCommand.create("large wall");

        assertEquals("create did not create correct LookCommand with multiple word target.",
                "large wall", lookCommand.getTarget().get());
    }

    @Test
    public void create_noTarget_createsCorrectCommand() {
        LookCommand lookCommand = LookCommand.create("");

        assertFalse("create did not create LookCommand with no target.",
                lookCommand.getTarget().isPresent());
    }
}
