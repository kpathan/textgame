package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

import java.util.function.Function;

abstract class ManageItemCommand implements Command {
    private final String itemIdentifier;
    private final String usage;

    protected ManageItemCommand(String itemIdentifier, String usage) {
        this.itemIdentifier = itemIdentifier;
        this.usage = usage;
    }

    public String getItemIdentifier() {
        return itemIdentifier;
    }

    protected static <T extends Command> CommandFactory<String, T> factoryFor(Class<T> commandType,
                                                                           Function<String, T> constructor) {
        return (paramString -> {
            if (paramString.trim().isEmpty()) {
                throw new InvalidCommandException(GameUtility.usageMessage(paramString));
            }

            return constructor.apply(paramString);
        });
    }
}
