package khyri.textgame.core.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;

import java.io.InputStream;
import java.util.Map;
import java.util.function.BiConsumer;

public final class JsonIncluder {
    private final Gson gson;
    final String FILE_REFERENCE_KEY = "$fileRef";

    @Inject
    public JsonIncluder(Gson gson) {
        this.gson = gson;
    }

    public String resolveFileReferences(String json) {
        JsonObject fullJson = gson.fromJson(json, JsonElement.class).getAsJsonObject();

        visit(fullJson, FILE_REFERENCE_KEY, this::mergeObject);

        return gson.toJson(fullJson);
    }

    private void mergeObject(String fileFrom, JsonObject to) {
        to.remove(FILE_REFERENCE_KEY);
        InputStream fileStream = getClass().getResourceAsStream(fileFrom);
        if (fileStream == null) {
            throw new IllegalArgumentException(
                    String.format("The referenced json file \"%s\" could not be found.", fileFrom));
        }
        String fileContentsString = FileUtility.readAllContent(fileStream);

        mergeObject(gson.fromJson(fileContentsString, JsonElement.class).getAsJsonObject(), to);
    }

    private void mergeObject(JsonObject from, JsonObject to) {
        for (Map.Entry<String, JsonElement> element :
                from.entrySet()) {
            to.add(element.getKey(), element.getValue());
        }
    }

    private static void dispatch(JsonElement jsonElement, String searchProperty, BiConsumer<String, JsonObject> action) {
        if (jsonElement.isJsonObject()) {
            visit(jsonElement.getAsJsonObject(), searchProperty, action);
        } else if (jsonElement.isJsonArray()) {
            visit(jsonElement.getAsJsonArray(), searchProperty, action);
        }
    }

    private static void visit(JsonObject jsonObject, String searchProperty, BiConsumer<String, JsonObject> action) {
        if (jsonObject.has(searchProperty)) {
            action.accept(jsonObject.get(searchProperty).getAsString(), jsonObject);
            return;
        }

        for (Map.Entry<String, JsonElement> property :
                jsonObject.entrySet()) {
            dispatch(property.getValue(), searchProperty, action);
        }
    }

    private static void visit(JsonArray jsonArray, String searchProperty, BiConsumer<String, JsonObject> action) {
        for (JsonElement element :
                jsonArray) {
            dispatch(element, searchProperty, action);
        }
    }

}
