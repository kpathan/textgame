package khyri.textgame.core;

import khyri.textgame.core.util.Node;
import org.junit.Test;

import java.util.Collections;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RoomViewTest {
    @Test
    public void render_handlesMultipleEdgesProperly() {
        Node<Room> roomNode = new Node<>(
                TestUtility.testRoom("test",
                        "Description goes here.",
                        Collections.singletonList(TestUtility.testItem("sword")),
                        Collections.emptyList(),
                        Collections.singletonList(TestUtility.testNpc("Bob")))
        );
        roomNode.connectBidirectionallyTo(new Node<>(TestUtility.testRoom("test2")));
        roomNode.connectBidirectionallyTo(new Node<>(TestUtility.testRoom("test3")));
        RoomView roomView = new RoomView();

        String result = roomView.render(roomNode);

        String possibleOutput1 = "\nYou are in test.\n" +
                "Description goes here.\n" +
                "Items: (sword)\n" +
                "There are people here: (Bob)\n" +
                "There are 2 exits:" +
                "\n> test2" +
                "\n> test3";
        String possibleOutput2 = "\nYou are in test.\n" +
                "Description goes here.\n" +
                "Items: (sword)\n" +
                "There are people here: (Bob)\n" +
                "There are 2 exits:" +
                "\n> test3" +
                "\n> test2";

        boolean isCorrectOutput = Objects.equals(result, possibleOutput1) || Objects.equals(result, possibleOutput2);

        assertTrue("render returned wrong String when given multiple edges.", isCorrectOutput);
    }

    @Test
    public void render_handlesSingleEdgeProperly() {
        Node<Room> roomNode = new Node<>(
                TestUtility.testRoom("test",
                        "Description goes here.",
                        Collections.singletonList(TestUtility.testItem("sword")),
                        Collections.emptyList(),
                        Collections.singletonList(TestUtility.testNpc("Bob")))
        );
        roomNode.connectBidirectionallyTo(new Node<>(TestUtility.testRoom("test2")));
        RoomView roomView = new RoomView();

        String result = roomView.render(roomNode);

        assertEquals("render returned wrong String when given single edge.",
                "\nYou are in test.\n" +
                        "Description goes here.\n" +
                        "Items: (sword)\n" +
                        "There are people here: (Bob)\n" +
                        "There is 1 exit:" +
                        "\n> test2", result);
    }
}
