package khyri.textgame.core.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class NodeDeserializer<T> implements JsonDeserializer<Node<T>> {
    private final Type dataType;
    private final Function<T, String> getIdentifier;
    private final BiConsumer<Node<T>, Node<T>> connector;

    public NodeDeserializer(Class<T> dataType, Function<T, String> getIdentifier, BiConsumer<Node<T>, Node<T>> connector) {
        this.dataType = dataType;
        this.getIdentifier = getIdentifier;
        this.connector = connector;
    }
    @Override
    public Node<T> deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {

        Type graphDescriptionType = getGraphDescriptionType();

        GraphDescription<T> graphDescription =
                jsonDeserializationContext.deserialize(jsonElement, graphDescriptionType);

        return new GraphBuilder<>(graphDescription, getIdentifier, connector).buildNodes();
    }

    private ParameterizedType getGraphDescriptionType() {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] { dataType };
            }

            @Override
            public Type getRawType() {
                return GraphDescription.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }
}
