package khyri.textgame.core.events;

import khyri.textgame.core.Item;
import khyri.textgame.core.triggers.Trigger;

import java.util.ArrayList;
import java.util.List;

public class PlayerGotItemEvent {
    private final Item itemReceived;
    private final List<Trigger> triggers;

    public PlayerGotItemEvent(Item itemReceived, List<Trigger> triggers) {
        this.itemReceived = itemReceived;
        this.triggers = new ArrayList<>(triggers);
    }

    public Item getItemReceived() {
        return itemReceived;
    }

    public List<Trigger> getTriggers() {
        return new ArrayList<>(triggers);
    }
}
