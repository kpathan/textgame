package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

import java.util.Objects;

public class MoveCommand implements Command {
    private final String target;

    private MoveCommand(String target) {
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public static MoveCommand create(String paramString)
            throws InvalidCommandException {
        String[] params = paramString.split("\\s+", 2);

        final String USAGE = "move to location";
        if(params.length < 2) {
            throw new InvalidCommandException(GameUtility.usageMessage(USAGE));
        }

        if(!Objects.equals(params[0], "to")) {
            throw new InvalidCommandException(GameUtility.usageMessage(USAGE));
        }

        return new MoveCommand(params[1]);
    }
}
