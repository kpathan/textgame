package khyri.textgame.core.commands;

import khyri.textgame.core.util.GameUtility;

public class GameEndCommand implements Command {
    public static GameEndCommand create(String paramString)
            throws InvalidCommandException {
        if(!paramString.equals("")) {
            throw new InvalidCommandException(GameUtility.usageMessage("exit | quit"));
        }

        return new GameEndCommand();
    }
}
