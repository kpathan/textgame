package khyri.textgame.core.commands;

@FunctionalInterface
public interface CommandFactory<String, T extends Command> {
    T create(String paramString) throws InvalidCommandException;
}
