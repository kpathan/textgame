package khyri.textgame.core;

import java.util.Objects;

public abstract class GameValue {
    private final String identifier;
    private final String description;

    public GameValue(String identifier,
                     String description) {
        this.identifier = identifier;
        this.description = description;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public int hashCode() {
        return this.identifier.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof GameValue)) {
            return false;
        }

        if(other == this) {
            return true;
        }

        GameValue otherRoom = (GameValue) other;

        return Objects.equals(this.getIdentifier(), otherRoom.getIdentifier());
    }
}
