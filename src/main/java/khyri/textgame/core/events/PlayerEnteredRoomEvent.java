package khyri.textgame.core.events;

import khyri.textgame.core.triggers.Trigger;

import java.util.ArrayList;
import java.util.List;

public class PlayerEnteredRoomEvent {
    private final List<Trigger> triggers;

    public PlayerEnteredRoomEvent(List<Trigger> triggers){
        this.triggers = triggers;
    }

    public List<Trigger> getTriggers() {
        return new ArrayList<>(triggers);
    }
}
