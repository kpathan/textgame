package khyri.textgame.core.components;

import com.google.common.eventbus.Subscribe;
import khyri.textgame.core.GameComponent;
import khyri.textgame.core.events.GameStartEvent;
import khyri.textgame.core.util.GameUtility;

public class DefaultEventHandlers extends GameComponent {
    @Subscribe
    void onGameStart(GameStartEvent gameStartEvent) {
        GameUtility.displayRoom(getEventBus(), getWorld());
    }
}
