package khyri.textgame.core.util;

import khyri.textgame.core.Room;
import khyri.textgame.core.TestUtility;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class GraphBuilderTest {
    @Test
    public void buildNodes_createsValidGraph() {
        GraphDescription<Room> graphDescription = new GraphDescription<>(
                Arrays.asList(TestUtility.testRoom("Field"), TestUtility.testRoom("Cave")),
                Collections.singletonList(new Edge("Field", "Cave")), "Field");

        GraphBuilder<Room> graphBuilder = new GraphBuilder<>(graphDescription,
                Room::getIdentifier,
                Node::connectBidirectionallyTo);

        Node<Room> graph = graphBuilder.buildNodes();

        assertTrue("buildNodes returned incorrect graph structure.",
                graph.getData().getIdentifier().equals("Field"));
    }

    @Test
    public void buildNodes_moreComplexGraph_createsValidGraph() {
        GraphDescription<Room> graphDescription = new GraphDescription<>(
                Arrays.asList(TestUtility.testRoom("Field"),
                        TestUtility.testRoom("Cave"),
                        TestUtility.testRoom("Inner Cave"),
                        TestUtility.testRoom("Farmhouse")),
                Arrays.asList(new Edge("Field", "Cave"),
                        new Edge("Field", "Farmhouse"),
                        new Edge("Cave", "Inner Cave")), "Field");

        GraphBuilder<Room> graphBuilder = new GraphBuilder<>(graphDescription,
                Room::getIdentifier,
                Node::connectBidirectionallyTo);

        Node<Room> graph = graphBuilder.buildNodes();

        assertTrue("buildNodes returned incorrect graph structure.",
                graph.getData().getIdentifier().equals("Field"));

        assertTrue("buildNodes returned incorrect graph structure.",
                graph.findConnectedNode(TestUtility.testRoom("Cave"))
                        .get()
                        .findConnectedNode(TestUtility.testRoom("Inner Cave"))
                        .isPresent());

        assertTrue("buildNodes returned incorrect graph structure.",
                graph.findConnectedNode(TestUtility.testRoom("Farmhouse"))
                        .isPresent());
    }

    @Test
    public void buildNodes_startParameterIsIdentifierOfStartNode() {
        GraphDescription<Room> graphDescription =
                new GraphDescription<>(
                        Arrays.asList(TestUtility.testRoom("Field"),
                                TestUtility.testRoom("Cave")),
                        Collections.singletonList(new Edge("Field", "Cave")),
                        "Cave"
                );

        GraphBuilder<Room> graphBuilder =
                new GraphBuilder<>(graphDescription,
                        Room::getIdentifier,
                        Node::connectBidirectionallyTo);

        Node<Room> graph = graphBuilder.buildNodes();

        assertEquals("Start node is not Cave.", graph.getData().getIdentifier(), "Cave");
    }
}
