package khyri.textgame.core.commands;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TextCommandSourceTest {
    @Test
    public void next_findsCommandInRegistry()
            throws IOException, InvalidCommandException {
        //arrange
        Map<String, CommandFactory<String, ? extends Command>> commandRegistry =
                new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        commandRegistry.put("move", MoveCommand::create);

        Command returnedCommand;
        try(InputStream stream = new ByteArrayInputStream("move to a cave".getBytes());
            Scanner scanner = new Scanner(stream)) {
            TextCommandSource source =
                    new TextCommandSource(commandRegistry, scanner);

            //act
            returnedCommand = source.next();
        }

        //assert
        assertTrue("next did not find the valid command \"move\"",
                returnedCommand instanceof MoveCommand);
    }

    @Test
    public void next_parameterLessCommand_doesNotThrow()
            throws IOException, InvalidCommandException {
        //arrange
        Map<String, CommandFactory<String, ? extends Command>> commandRegistry =
                new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        commandRegistry.put("noop", input -> null);

        Command returnedCommand;
        try(InputStream stream = new ByteArrayInputStream("noop".getBytes());
            Scanner scanner = new Scanner(stream)) {
            TextCommandSource source =
                    new TextCommandSource(commandRegistry, scanner);

            //act
            source.next();
        }

        //assert -- if we made it this far, there were no exceptions
        assertTrue(true);
    }

    @Test
    public void next_trimsWhiteSpaceAfterCommand()
            throws IOException, InvalidCommandException {
        //arrange
        Map<String, CommandFactory<String, ? extends Command>> commandRegistry =
                new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        commandRegistry.put("noop", input -> {
            assertTrue("next did not trim whitespace after parameter-less command", input.isEmpty());
            return null;
        });

        Object returnedCommand;
        try(InputStream stream = new ByteArrayInputStream("noop   ".getBytes());
            Scanner scanner = new Scanner(stream)) {
            TextCommandSource source =
                    new TextCommandSource(commandRegistry, scanner);

            //act
            source.next();
        }
    }

    @Test
    public void next_trimsWhiteSpaceBeforeParams()
            throws IOException, InvalidCommandException {
        //arrange
        Map<String, CommandFactory<String, ? extends Command>> commandRegistry =
                new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        commandRegistry.put("noop", input -> {
            assertEquals("test", input);
            return null;
        });

        Object returnedCommand;
        try(InputStream stream = new ByteArrayInputStream("noop    test".getBytes());
            Scanner scanner = new Scanner(stream)) {
            TextCommandSource source =
                    new TextCommandSource(commandRegistry, scanner);

            //act
            source.next();
        }
    }
}
