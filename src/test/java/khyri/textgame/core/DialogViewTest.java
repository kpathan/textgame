package khyri.textgame.core;

import khyri.textgame.core.util.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DialogViewTest {
    @Test
    public void render_createsCorrectSting() {
        Dialog.Choice choice1 = new Dialog.Choice("Buzz off.");
        Dialog.Choice choice2 = new Dialog.Choice("Carry on then.", "Ok.");

        Node<Dialog.Choice> choices = new Node<>(choice1);
        choices.connectBidirectionallyTo(new Node<>(choice2));

        DialogChoiceView dialogChoiceView = new DialogChoiceView();

        String result = dialogChoiceView.render(choices);

        String expected = "Buzz off.\n> Ok.";

        assertEquals("render did not create correct String.", expected, result);
    }
}
